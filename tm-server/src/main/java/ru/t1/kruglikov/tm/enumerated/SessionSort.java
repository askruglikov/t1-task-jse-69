package ru.t1.kruglikov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum SessionSort {

    BY_LAST_DATE("Sort by last date", "LAST_DATE");


    @NotNull
    private final String name;

    @NotNull
    private final String columnName;

    SessionSort(
            @NotNull final String name,
            @NotNull final String columnName
    ) {
        this.name = name;
        this.columnName = columnName;
    }

    @Nullable
    public static SessionSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final SessionSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}

