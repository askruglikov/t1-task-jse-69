package ru.t1.kruglikov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    List<Project> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    );

    @NotNull
    List<Project> findAll(
            @Nullable final ProjectSort sort
    );

    @NotNull
    Project create(
            @Nullable final String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project updateById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

}
