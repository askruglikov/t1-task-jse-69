package ru.t1.kruglikov.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.cfg.Environment;
import ru.t1.kruglikov.tm.api.service.IPropertyService;


import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan("ru.t1.kruglikov.tm")
@EnableTransactionManagement
@EnableJpaRepositories("ru.t1.kruglikov.tm.api.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseLogin());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager (
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.kruglikov.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHBM2DDLAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSQL());
        properties.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSQL());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseSecondLvlCash());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseQueryCash());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseConfigFilePath());
        return factoryBean;
    }

}
