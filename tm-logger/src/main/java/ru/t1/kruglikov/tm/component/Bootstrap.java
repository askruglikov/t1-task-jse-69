package ru.t1.kruglikov.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.listener.EntityListener;
import ru.t1.kruglikov.tm.service.EntityService;

import javax.jms.*;

@Component
public final class Bootstrap {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    private static final String QUEUE = "LOGGER";

    @NotNull
    @Autowired
    private EntityService entityService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @SneakyThrows
    public void start() {

        @NotNull
        final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull
        final Queue destination = session.createQueue(QUEUE);
        @NotNull
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
