package ru.kruglikov.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kruglikov.tm.api.client.ITaskEndpointClient;
import ru.kruglikov.tm.marker.IntegrationCategory;
import ru.kruglikov.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    final static String BASE_URL = "http://localhost:8080/api/tasks";

    @NotNull
    private ITaskEndpointClient taskClient = ITaskEndpointClient.taskClient(BASE_URL);

    @NotNull
    private Collection<Task> tasks = new ArrayList<>();

    @NotNull
    private final Task taskOne = new Task("TestTaskOne");

    @NotNull
    private final Task taskTwo = new Task("TestTaskTwo");

    @NotNull
    private final Task taskTree = new Task("TestTaskTree");

    @Before
    public void init() {
        taskClient.save(taskOne);
        taskClient.save(taskTwo);
        taskClient.save(taskTree);
        tasks = taskClient.findAll();
    }

    @After
    public void clean() {
        taskClient.delete(taskOne);
        taskClient.delete(taskTwo);
        taskClient.delete(taskTree);
    }

    @Test
    public void findById() {
        Task task = taskClient.findById(taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskOne.getId());
    }

    @Test
    public void findAll() {
        Collection<Task> findTask = taskClient.findAll();
        Assert.assertEquals(tasks.size(), findTask.size());
    }

    @Test
    public void save() {
        @NotNull final Task task = new Task("TestProjectFour");
        taskClient.save(task);
        String newTaskId = task.getId();
        Task findedTask = taskClient.findById(newTaskId);
        Assert.assertNotNull(findedTask);
        tasks.add(task);
    }

    @Test
    public void delete() {
        Random random = new Random();
        int randIndex = random.nextInt(taskClient.findAll().size() - 1);
        ArrayList<Task> testTasks = (ArrayList<Task>) taskClient.findAll();
        Task randTask = testTasks.get(randIndex);
        String randTaskId = randTask.getId();
        taskClient.delete(randTask);
        Assert.assertNull(taskClient.findById(randTaskId));
        tasks.remove(randTask);
    }

    @Test
    public void deleteById() {
        Random random = new Random();
        int randIndex = random.nextInt(taskClient.findAll().size() - 1);
        ArrayList<Task> testTasks = (ArrayList<Task>) taskClient.findAll();
        Task randTask = testTasks.get(randIndex);
        String randTaskId = randTask.getId();
        taskClient.delete(randTaskId);
        Assert.assertNull(taskClient.findById(randTaskId));
        tasks.remove(randTask);
    }

}