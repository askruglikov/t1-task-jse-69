package ru.kruglikov.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kruglikov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException(@NotNull final String message) {
        super(message);
    }

    public AbstractFieldException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}