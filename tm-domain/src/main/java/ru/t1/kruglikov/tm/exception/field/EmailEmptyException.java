package ru.t1.kruglikov.tm.exception.field;

public class EmailEmptyException extends AbstractFieldExceprion {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
