package ru.t1.kruglikov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.model.IWBS;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.exception.entity.UserNotFoundException;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TM_TASK")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(name = "NAME")
    private String name = "";

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description = "";

    @NotNull
    @Column(name = "STATUS")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "PROJECT_ID")
    private Project project;

    @NotNull
    @Column(name = "CREATED")
    private Date created = new Date();

    public Task(@Nullable final User user,
                @NotNull final String name,
                @NotNull final String description,
                @NotNull final Status status
    ) {
        if (user == null) throw new UserNotFoundException();
        this.setUser(user);
        this.name = name;
        this.description = description;
        this.status = status;
    }

}