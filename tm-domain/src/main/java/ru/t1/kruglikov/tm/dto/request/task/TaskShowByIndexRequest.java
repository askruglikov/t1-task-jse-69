package ru.t1.kruglikov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskShowByIndexRequest(@Nullable String token) {
        super(token);
    }

}
